package minicad;

import java.awt.*;

public class Cir extends Point implements dibu {

    private int radio;
    private Color color;

    public Cir(int x, int y, int radio, Color color) {
        super(x, y);
        this.radio = radio;
        this.color = color;
    }

    public Cir() {
        this(0, 0, 0, Color.BLACK);
    }

    @Override
    public void dibujar(Graphics g) {
        g.setColor(color);
        g.fillOval(x-25 - radio, y-75 - radio+50, 2 * radio+50, 2 * radio+50);
        g.setColor(Color.BLACK);
        g.drawOval(x-25 - radio, y-75 - radio+50, 2 * radio+50, 2 * radio+50);
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}

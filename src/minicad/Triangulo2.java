package minicad;

import java.awt.Graphics;
import java.util.ArrayList;

public class Triangulo2  extends ArrayList<Triangulo> implements dibu{
    
     @Override
  public void dibujar(Graphics g) {
      for (Triangulo triangulo : this) {
          triangulo.dibujar(g);
      }
  }
    
}

package minicad;
import java.awt.Graphics;
import java.util.ArrayList;

public class Rectangulos extends ArrayList<Rectangulo> implements dibu {

  @Override
  public void dibujar(Graphics g) {
      for (Rectangulo cuadrado : this) {
          cuadrado.dibujar(g);
      }
  }
}

package minicad;

import java.awt.Graphics;
import java.util.ArrayList;

public class Circu extends ArrayList<Cir> implements dibu {

  @Override
  public void dibujar(Graphics g) {
      for (Cir circulo : this) {
          circulo.dibujar(g);
      }
  }
}

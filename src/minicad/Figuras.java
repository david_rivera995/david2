package minicad;

import javax.swing.JFrame;

public class Figuras {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Circu circulos = new Circu();
        Rectangulos cuadrados = new Rectangulos();
        Triangulo2 triangulos = new Triangulo2();
        PanelFiguras panel = new PanelFiguras(triangulos,cuadrados,circulos);
        Controlador controlador = new Controlador(triangulos,cuadrados,circulos, panel);
        panel.addEventos(controlador);
        JFrame f = new JFrame("");
        f.setSize(1200, 700);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    }
    
}
